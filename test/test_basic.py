import unittest
class Chat:
    def __init__(self,nom:str,couleur:str) -> None:
        self.nom = nom
        self.couleur = couleur
    def is_roux(self) -> bool:
        return True if self.couleur == 'roux' else False
    
class TestChat(unittest.TestCase):

    def test_is_roux_ok(self):
        chat = Chat('felix','roux')
        self.assertTrue(chat.is_roux())
       
    def test_is_roux_shouldBeFalse(self):
        chat = Chat('felix','noir')
        self.assertFalse(chat.is_roux()) 
